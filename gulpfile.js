var gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    babel = require('gulp-babel');

const SRC_ROOT_DIRECTORY = './src/main/resources';

gulp.task('default', ['js'], function() {
    return gulp.src([SRC_ROOT_DIRECTORY + '/**/*.scss'])
           .pipe(concat('index.scss'))
           .pipe(sass().on('error', sass.logError))
           .pipe(gulp.dest('./target/classes/static/css'));
});

gulp.task('js', function () {
    return gulp.src([SRC_ROOT_DIRECTORY + '/**/*.js', SRC_ROOT_DIRECTORY + '/**/*.jsx'])
        .pipe(babel({
            presets: ['es2015', 'react']
        }))
        .pipe(concat('index.js'))
        .pipe(gulp.dest('./target/classes/static/js'));
})