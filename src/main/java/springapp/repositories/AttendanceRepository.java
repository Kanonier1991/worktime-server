package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.Attendance;
import springapp.dto.Employee;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Igor on 12.04.2017.
 */
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {

    List<Attendance> findAttendancesByEmployeeAndStartAfterAndFinishBeforeOrderByStart(Employee employee, LocalDateTime start, LocalDateTime finish);
    List<Attendance> findAttendancesByEmployeeAndFinish(Employee employee, LocalDateTime finish);
}
