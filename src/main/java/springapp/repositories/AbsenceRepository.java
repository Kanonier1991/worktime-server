package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.Absence;
import springapp.dto.AbsenceReason;
import springapp.dto.Employee;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Igor on 12.04.2017.
 */
public interface AbsenceRepository extends JpaRepository<Absence, Long> {

    List<Absence> findAbsencesByEmployeeAndStartAfterAndFinishBefore(Employee employee, LocalDateTime start, LocalDateTime finish);

    List<Absence> findAbsencesByEmployeeAndReasonAndStartAfterAndFinishBefore(Employee employee, AbsenceReason reason, LocalDateTime start, LocalDateTime finish);
}
