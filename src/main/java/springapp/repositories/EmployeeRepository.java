package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.Employee;

import java.util.List;

/**
 * Created by Igor on 13.04.2017.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByIp(String ip);
    List<Employee> findByActive(boolean active);
}
