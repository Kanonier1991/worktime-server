package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.Employee;
import springapp.dto.ExternalAttendance;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Igor on 21.05.2017.
 */
public interface ExternalAttendanceRepository extends JpaRepository<ExternalAttendance, Long> {

    List<ExternalAttendance> findByEmployeeAndStartAndFinish(Employee employee, LocalDateTime start, LocalDateTime finish);
    List<ExternalAttendance> findByEmployeeAndStartBetween(Employee employee, LocalDateTime start, LocalDateTime finish);
    List<ExternalAttendance> findByEmployeeAndFinishBetween(Employee employee, LocalDateTime start, LocalDateTime finish);
}
