package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.Position;

/**
 * Created by Igor on 12.04.2017.
 */
public interface PositionRepository extends JpaRepository<Position, Long> {
}
