package springapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import springapp.dto.AbsenceReason;

/**
 * Created by Igor on 13.04.2017.
 */
public interface AbsenceReasonRepository extends JpaRepository<AbsenceReason, Long> {
}
