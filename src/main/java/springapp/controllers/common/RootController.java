package springapp.controllers.common;

import static springapp.core.Constants.ADMIN_IP;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springapp.dto.AbsenceReason;
import springapp.dto.Employee;
import springapp.dto.Position;
import springapp.dto.view.TimeJournal;
import springapp.dto.view.TimeTable;
import springapp.repositories.AbsenceReasonRepository;
import springapp.repositories.EmployeeRepository;
import springapp.repositories.PositionRepository;
import springapp.services.core.AttendanceService;
import springapp.services.core.EmployeeService;
import springapp.services.importer.DocumentParser;
import springapp.services.importer.TimeImporter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static springapp.core.Utils.getDatesOfWeek;

/**
 * Created by Igor on 16.04.2017.
 */
@Controller
public class RootController {


    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("E, dd-MM");

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private AbsenceReasonRepository absenceReasonRepository;
    @Autowired
    private AttendanceService attendanceService;
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TimeImporter timeImporter;
    @Autowired
    private DocumentParser documentParser;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String getMainPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!ADMIN_IP.equals(request.getRemoteAddr())) {
            response.sendRedirect("/account");
        }
        return "index";
    }

    @RequestMapping(path = "/employee/create", method = RequestMethod.GET)
    public String getEmployesPage(Model model) {
        model.addAttribute("employes", employeeRepository.findAll());
        model.addAttribute("positions", positionRepository.findAll());
        return "createemployee";
    }

    @RequestMapping(path = "/position/create", method = RequestMethod.GET)
    public String getPositionsPage(Model model) {
        model.addAttribute("positions", positionRepository.findAll());
        return "createposition";
    }

    @RequestMapping(path = "/position/create", method = RequestMethod.POST)
    public void createPosition(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String shortName = request.getParameter("shortName");
        String fullName = request.getParameter("fullName");
        Position position = new Position(shortName, fullName);
        positionRepository.save(position);
        response.sendRedirect("/position/create");
    }

    @RequestMapping(path = "/absence/create", method = RequestMethod.GET)
    public String getAbcensePage(Model model) {
        model.addAttribute("reasons", absenceReasonRepository.findAll());
        return "createabsencereason";
    }

    @RequestMapping(path = "/absence/create", method = RequestMethod.POST)
    public void createAbsenceReason(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        boolean fullDay = "on".equals(request.getParameter("fullDay"));
        AbsenceReason absenceReason = new AbsenceReason(name, fullDay);
        absenceReasonRepository.save(absenceReason);
        response.sendRedirect("/absence/create");
    }

    @RequestMapping(path = "/account", method = RequestMethod.GET)
    public String getAccount(HttpServletRequest request, Model model) {
        Employee employee = employeeRepository.findByIp(request.getRemoteAddr());
        model.addAttribute("employee", employee);
        model.addAttribute("currentAttendance", attendanceService.getTodaysAttendances(request));
        List<TimeTable> timeTables = employeeService.getEmployeeTimeJournal(employee, LocalDate.now());
        List<String> timeHead = getDatesOfWeek(LocalDate.now()).stream()
                .map(localDate -> localDate.format(DATE_TIME_FORMATTER))
                .collect(Collectors.toList());
        TimeJournal timeJournal = new TimeJournal(timeHead, timeTables);
        model.addAttribute("accountTimeJournal", new Gson().toJson(timeJournal));
        return "account";
    }
}
