package springapp.controllers.common;

import static springapp.core.Utils.getDatesOfWeek;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springapp.dto.Employee;
import springapp.dto.view.TimeTable;
import springapp.services.core.EmployeeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Created by Igor on 30.04.2017.
 */
@Controller
@RequestMapping(path = "/employee")
public class EmployeeController {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("E, dd-MM");

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(path = "/add_reason", method = RequestMethod.POST)
    public void addReasonToEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Long employeeId = Long.valueOf(request.getParameter("empId"));
        Long reasonId = Long.valueOf(request.getParameter("reasonId"));
        employeeService.addReasonToEmployee(employeeId, reasonId);
        response.sendRedirect("/employee/create");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public String getEmployeeInfo(HttpServletRequest request, Model model) {
        Long empId = Long.valueOf(request.getParameter("empid"));
        Employee employee = employeeService.findByIdOrIp(empId, null);
        LocalDate date;
        String week = request.getParameter("week") == null ? "current" : request.getParameter("week");
        switch (week) {
            case "prev":
                date = LocalDate.now().minusDays(7);
                break;
            case "next":
                date = LocalDate.now().plusDays(7);
                break;
            case "current":
                date = LocalDate.now();
                break;
            default:
                int year = Integer.parseInt(week.substring(0,4));
                int weekNumber = Integer.parseInt(week.substring(6,8));
                WeekFields weekFields = WeekFields.of(Locale.getDefault());
                date = LocalDate.now().withYear(year)
                        .with(weekFields.weekOfYear() , weekNumber + 1)
                        .with(weekFields.dayOfWeek(), 1);
                break;
        }
        List<TimeTable> timeJournal = employeeService.getEmployeeTimeJournal(employee, date);
        List<String> timeHead = getDatesOfWeek(date).stream()
                                    .map(localDate -> localDate.format(DATE_TIME_FORMATTER))
                                    .collect(Collectors.toList());
        model.addAttribute("employee", employee);
        model.addAttribute("timeHeader", timeHead);
        model.addAttribute("timeJournal", timeJournal);
        return "userinfo";
    }

    @RequestMapping(path = "/absence/add", method = RequestMethod.POST)
    public void addAbsence(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Long employeeId = Long.valueOf(request.getParameter("empId"));
        Long reasonId = Long.valueOf(request.getParameter("reason"));
        LocalDateTime from = LocalDateTime.parse(request.getParameter("from"));
        LocalDateTime to = LocalDateTime.parse(request.getParameter("to"));
        employeeService.addAbsenceForEmployee(employeeId, reasonId, from, to);
        response.sendRedirect("/account");
    }
}
