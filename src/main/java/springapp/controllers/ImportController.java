package springapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springapp.services.importer.ImportScheduler;
import springapp.services.importer.TimeImporter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by Igor on 23.05.2017.
 */
@Controller
public class ImportController {

    @Autowired
    private TimeImporter timeImporter;

    @RequestMapping(path = "/import", method = RequestMethod.GET)
    public String getImport() throws IOException {
        return "import";
    }

    @RequestMapping(path = "/import", method = RequestMethod.POST)
    public String postImport(HttpServletRequest request) throws IOException {
        LocalDate start = LocalDate.parse(request.getParameter("start"));
        LocalDate finish = LocalDate.parse(request.getParameter("finish"));
        timeImporter.doImportAll(start, finish);
        return "import";
    }
}
