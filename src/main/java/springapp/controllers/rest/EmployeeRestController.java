package springapp.controllers.rest;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springapp.dto.Absence;
import springapp.dto.AbsenceReason;
import springapp.dto.Employee;
import springapp.dto.view.TimeJournal;
import springapp.dto.view.TimeTable;
import springapp.repositories.AbsenceReasonRepository;
import springapp.repositories.AbsenceRepository;
import springapp.repositories.EmployeeRepository;
import springapp.repositories.PositionRepository;
import springapp.services.core.EmployeeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import static springapp.core.Utils.getDatesOfWeek;


/**
 * Created by Igor on 13.04.2017.
 */
@RestController
@RequestMapping(path = "/employee")
public class EmployeeRestController {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("E, dd-MM");
    private static final DateTimeFormatter ABSENCE_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy:MM:dd");

    @Autowired
    private EmployeeRepository repository;
    @Autowired
    private AbsenceReasonRepository reasonRepository;
    @Autowired
    private AbsenceRepository absenceRepository;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(path = "/get_all")
    public List<Employee> getAll(HttpServletRequest request) {
        return repository.findAll();
    }

    @RequestMapping(path = "/get")
    public Employee getById(HttpServletRequest request) {
        Long employeeId = Long.valueOf(request.getParameter("employeeId"));
        Employee employee = repository.findOne(employeeId);
        return employee;
    }

    @RequestMapping(path = "/update_absence", method = RequestMethod.POST)
    public Long updateAbsence(HttpServletRequest request) {
        boolean addAbsence = Boolean.parseBoolean(request.getParameter("addAbsence"));

        if (addAbsence) {
            Long reasonId = Long.valueOf(request.getParameter("reasonId"));
            String absenceDate = request.getParameter("absenceDate");
            AbsenceReason absenceReason = reasonRepository.findOne(reasonId);
            Absence absence = new Absence();
            String[] dateParts = absenceDate.split(":");
            int year = Integer.parseInt(dateParts[0]);
            int month = Integer.parseInt(dateParts[1]);
            int day = Integer.parseInt(dateParts[2]);
            absence.setReason(absenceReason);
            absence.setEmployee(repository.findByIp(request.getRemoteAddr()));
            LocalDateTime from = LocalDate.of(year, month, day).atTime(8, 0);
            LocalDateTime to = LocalDate.of(year, month, day).atTime(18, 0);
            absence.setStart(from);
            absence.setFinish(to);
            absenceRepository.save(absence);
            return absence.getId();
        } else {
            Long absenceId = Long.valueOf(request.getParameter("absenceId"));
            absenceRepository.delete(absenceId);
            return null;
        }
    }

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public void createEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String ip = request.getParameter("ip");
        Long positionid = Long.valueOf(request.getParameter("position"));
        String employeeId = request.getParameter("employeeId");
        String systemId = request.getParameter("systemId");
        String externalName = request.getParameter("externalName");
        String externalPass = request.getParameter("externalPass");
        Employee employee;
        if (StringUtils.isEmpty(employeeId)) {
            employee = new Employee(name, positionRepository.findOne(positionid), null, true, ip, null, systemId, externalName, externalPass);
        } else {
            employee = repository.findOne(Long.valueOf(employeeId));
            employee.setName(name);
            employee.setIp(ip);
            employee.setPosition(positionRepository.findOne(positionid));
            employee.setSystemId(systemId);
            employee.setExternalName(externalName);
            employee.setPassword(externalPass);
        }
        repository.save(employee);
        response.setStatus(200);
    }

    @RequestMapping(path = "/get_time_journal", method = RequestMethod.GET)
    public String getTimeJournal(HttpServletRequest request) {
        Employee employee = employeeService.findByIdOrIp(null, request.getRemoteAddr());
        LocalDate date;
        String week = request.getParameter("week");
        switch (week) {
            case "prev":
                date = LocalDate.now().minusDays(7);
                break;
            case "next":
                date = LocalDate.now().plusDays(7);
                break;
            case "current":
                date = LocalDate.now();
                break;
            default:
                int year = Integer.parseInt(week.substring(0,4));
                int weekNumber = Integer.parseInt(week.substring(6,8));
                WeekFields weekFields = WeekFields.of(Locale.getDefault());
                date = LocalDate.now().withYear(year)
                        .with(weekFields.weekOfYear() , weekNumber + 1)
                        .with(weekFields.dayOfWeek(), 1);
                break;
        }
        List<TimeTable> timeTables = employeeService.getEmployeeTimeJournal(employee, date);
        List<String> timeHead = getDatesOfWeek(date).stream()
                .map(localDate -> localDate.format(DATE_TIME_FORMATTER))
                .collect(Collectors.toList());
        return new Gson().toJson(new TimeJournal(timeHead, timeTables));
    }

    @RequestMapping(path = "/addReason", method = RequestMethod.POST)
    public Set<AbsenceReason> addReason(HttpServletRequest request) {
        Long empId = Long.valueOf(request.getParameter("empId"));
        Employee employee = repository.findOne(empId);
        Long reasonId = Long.valueOf(request.getParameter("reasonId"));
        AbsenceReason absenceReason = reasonRepository.findOne(reasonId);
        employee.getEmployeeReasons().add(absenceReason);
        repository.save(employee);
        return employee.getEmployeeReasons();
    }

    @RequestMapping(path = "/removeReason", method = RequestMethod.POST)
    public Set<AbsenceReason> removeReason(HttpServletRequest request) {
        Long empId = Long.valueOf(request.getParameter("empId"));
        Employee employee = repository.findOne(empId);
        Long reasonId = Long.valueOf(request.getParameter("reasonId"));
        AbsenceReason absenceReason = reasonRepository.findOne(reasonId);
        employee.getEmployeeReasons().remove(absenceReason);
        repository.save(employee);
        return employee.getEmployeeReasons();
    }

    @RequestMapping(path = "/get_absence_by_date", method = RequestMethod.GET)
    public List<TimeTable.TimeByDay.Time.TimePair> getAbsenceByDate(HttpServletRequest request) {
        Employee employee = employeeService.findByIdOrIp(null, request.getRemoteAddr());
        Long reasonId = Long.valueOf(request.getParameter("reasonId"));
        String absenceDate = request.getParameter("absenceDate");
        String[] partOfDate = absenceDate.split(":");
        AbsenceReason reason = reasonRepository.findOne(reasonId);
        LocalDate dateOfAbsence = LocalDate.of(
                Integer.valueOf(partOfDate[0]),
                Integer.valueOf(partOfDate[1]),
                Integer.valueOf(partOfDate[2])
        );
        List<Absence> absences = absenceRepository.findAbsencesByEmployeeAndReasonAndStartAfterAndFinishBefore(employee, reason, dateOfAbsence.atTime(LocalTime.MIN), dateOfAbsence.atTime(LocalTime.MAX));
        List<TimeTable.TimeByDay.Time.TimePair> timePairs = absences.stream()
                .map(absence -> {
                    TimeTable.TimeByDay.Time.TimePair timePair = new TimeTable.TimeByDay.Time.TimePair();
                    timePair.setFrom(absence.getStart());
                    timePair.setTo(absence.getFinish());
                    return timePair;
                })
                .collect(Collectors.toList());
        return timePairs;
    }

    @RequestMapping(path = "/add_temporal_absence", method = RequestMethod.POST)
    public String addTemporalAbsence(HttpServletRequest request) {
        int startHour = Integer.parseInt(request.getParameter("startHour"));
        int startMin = Integer.parseInt(request.getParameter("startMin"));
        int finishHour = Integer.parseInt(request.getParameter("finishHour"));
        int finishMin = Integer.parseInt(request.getParameter("finishMin"));
        String[] absenceDate = request.getParameter("absenceDate").split(":");
        Long reasonid = Long.valueOf(request.getParameter("reasonid"));

        LocalDate dateAbscence = LocalDate.of(
                Integer.valueOf(absenceDate[0]),
                Integer.valueOf(absenceDate[1]),
                Integer.valueOf(absenceDate[2])
        );
        Employee employee = employeeService.findByIdOrIp(null, request.getRemoteAddr());
        AbsenceReason reason = reasonRepository.findOne(reasonid);
        LocalDateTime start = dateAbscence.atTime(LocalTime.of(startHour, startMin));
        LocalDateTime finish = dateAbscence.atTime(LocalTime.of(finishHour, finishMin));
        Absence absence = new Absence(employee, start, finish, reason);
        absenceRepository.save(absence);
        List<TimeTable> timeTables = employeeService.getEmployeeTimeJournal(employee, dateAbscence);
        List<String> timeHead = getDatesOfWeek(dateAbscence).stream()
                .map(localDate -> localDate.format(DATE_TIME_FORMATTER))
                .collect(Collectors.toList());
        return new Gson().toJson(new TimeJournal(timeHead, timeTables));
    }
}
