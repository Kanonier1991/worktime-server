package springapp.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springapp.dto.Attendance;
import springapp.services.core.AttendanceService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Igor on 14.04.2017.
 */
@RestController
@RequestMapping(path = "/attendance")
public class AttendanceRestController {

    @Autowired
    private AttendanceService attendanceService;

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Attendance save(HttpServletRequest request) {
        return attendanceService.save(request);
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public List<Attendance> update(HttpServletRequest request) {
        return attendanceService.update(request);
    }

    @RequestMapping(path = "/lock_system", method = RequestMethod.POST)
    public List<Attendance> lockSystem(HttpServletRequest request) {
        return attendanceService.lockSystem(request.getRemoteAddr());
    }

    @RequestMapping(path = "/unlock_system", method = RequestMethod.POST)
    public Attendance unlockSystem(HttpServletRequest request) {
        return attendanceService.unlockSystem(request.getRemoteAddr());
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public List<Attendance> getAttendanceList(HttpServletRequest request) {
        /*ToDo*/
        /*List<Attendance> attendances = attendanceService.getAttendances(request);*/
        return null;
    }
}
