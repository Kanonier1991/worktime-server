package springapp.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springapp.dto.Position;
import springapp.repositories.PositionRepository;

import java.util.List;

/**
 * Created by Igor on 13.04.2017.
 */
@RestController
@RequestMapping(path = "/position")
public class PositionRestController {

    @Autowired
    private PositionRepository repository;

    @RequestMapping(path = "/get_all")
    public List<Position> getAll() {
        return repository.findAll();
    }
}
