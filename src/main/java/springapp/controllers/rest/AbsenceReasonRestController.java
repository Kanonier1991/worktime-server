package springapp.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springapp.dto.AbsenceReason;
import springapp.repositories.AbsenceReasonRepository;
import springapp.repositories.EmployeeRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * Created by Igor on 13.04.2017.
 */
@RestController
@RequestMapping(path = "/absencereason")
public class AbsenceReasonRestController {

    @Autowired
    private AbsenceReasonRepository repository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @RequestMapping(path = "/get_all", method = RequestMethod.GET)
    public List<AbsenceReason> getAll() {
        List<AbsenceReason> reasons = repository.findAll();
        return reasons;
    }

    @RequestMapping(path = "/get_by_employee", method = RequestMethod.GET)
    public Set<AbsenceReason> getByEmployee(HttpServletRequest request) {
        Long empId = Long.valueOf(request.getParameter("empId"));
        return employeeRepository.findOne(empId).getEmployeeReasons();
    }
}
