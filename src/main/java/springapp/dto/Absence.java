package springapp.dto;

import springapp.converters.DateTimeConverter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author Igor_Strakhov
 */
@Entity
public class Absence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime start;
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime finish;
    @ManyToOne
    @JoinColumn(name = "absenceReason_id")
    private AbsenceReason reason;

    public Absence() {
    }

    public Absence(Employee employee, LocalDateTime start, LocalDateTime finish, AbsenceReason reason) {
        this.employee = employee;
        this.start = start;
        this.finish = finish;
        this.reason = reason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public void setFinish(LocalDateTime finish) {
        this.finish = finish;
    }

    public AbsenceReason getReason() {
        return reason;
    }

    public void setReason(AbsenceReason reason) {
        this.reason = reason;
    }
}
