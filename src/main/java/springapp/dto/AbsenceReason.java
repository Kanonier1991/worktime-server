package springapp.dto;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Igor_Strakhov
 */
@Entity
public class AbsenceReason {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    private boolean fullDay;

    public AbsenceReason() {
    }

    public AbsenceReason(String name, boolean fullDay) {
        this.name = name;
        this.fullDay = fullDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFullDay() {
        return fullDay;
    }

    public void setFullDay(boolean fullDay) {
        this.fullDay = fullDay;
    }
}
