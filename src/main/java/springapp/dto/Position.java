package springapp.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Igor_Strakhov
 */
@Entity
public class Position {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long ID;
    private String shortName;
    private String fullName;

    public Position(String shortName, String fullName) {
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public Position() {
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
