package springapp.dto.importer;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by Igor on 21.05.2017.
 */
public class ImportingTime {


    public static final class Day {

        private LocalDate date;
        private List<Time> times;

        public Day() {
        }

        public Day(LocalDate date, List<Time> times) {
            this.date = date;
            this.times = times;
        }

        public LocalDate getDate() {
            return date;
        }

        public void setDate(LocalDate date) {
            this.date = date;
        }

        public List<Time> getTimes() {
            return times;
        }

        public void setTimes(List<Time> times) {
            this.times = times;
        }
    }

    public static final class Time {

        private LocalTime start;
        private LocalTime finish;

        public Time() {
        }

        public Time(LocalTime start, LocalTime finish) {
            this.start = start;
            this.finish = finish;
        }

        public LocalTime getStart() {
            return start;
        }

        public void setStart(LocalTime start) {
            this.start = start;
        }

        public LocalTime getFinish() {
            return finish;
        }

        public void setFinish(LocalTime finish) {
            this.finish = finish;
        }
    }

    public static final class AttendanceTime {

        private String type;
        private LocalTime time;

        public AttendanceTime() {
        }

        public AttendanceTime(String type, LocalTime time) {
            this.type = type;
            this.time = time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public LocalTime getTime() {
            return time;
        }

        public void setTime(LocalTime time) {
            this.time = time;
        }
    }
}
