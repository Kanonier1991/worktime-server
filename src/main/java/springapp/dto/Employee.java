package springapp.dto;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Igor_Strakhov
 */
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "position_id")
    private Position position;
    private String signatureUri;
    private boolean active;
    private String ip;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "employee_reason", joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "reason_id", referencedColumnName = "id"))
    private Set<AbsenceReason> employeeReasons;
    //For import
    private String systemId;
    private String externalName;
    private String password;

    public Employee() {
    }

    public Employee(String name, Position position, String signatureUri, boolean active, String ip, Set<AbsenceReason> employeeReasons, String systemId, String externalName, String password) {
        this.name = name;
        this.position = position;
        this.signatureUri = signatureUri;
        this.active = active;
        this.ip = ip;
        this.employeeReasons = employeeReasons;
        this.systemId = systemId;
        this.externalName = externalName;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long ID) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getSignatureUri() {
        return signatureUri;
    }

    public void setSignatureUri(String signatureUri) {
        this.signatureUri = signatureUri;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        active = active;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Set<AbsenceReason> getEmployeeReasons() {
        return employeeReasons;
    }

    public void setEmployeeReasons(Set<AbsenceReason> employeeReasons) {
        this.employeeReasons = employeeReasons;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getExternalName() {
        return externalName;
    }

    public void setExternalName(String externalName) {
        this.externalName = externalName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
