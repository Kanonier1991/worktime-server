package springapp.dto;

import springapp.converters.DateTimeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Igor_Strakhov
 */
@Entity
public class ExternalAttendance {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime start;
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime finish;
    private String Comment;

    public ExternalAttendance() {
    }

    public ExternalAttendance(Employee employee, LocalDateTime start, LocalDateTime finish, String comment) {
        this.employee = employee;
        this.start = start;
        this.finish = finish;
        Comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public void setFinish(LocalDateTime finish) {
        this.finish = finish;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }
}
