package springapp.dto.view;

import springapp.dto.AbsenceReason;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by Igor on 30.04.2017.
 */
public class TimeTable {

    private Reason reason;
    private TimeByDay timeByDay;

    public TimeTable(Reason reason, TimeByDay timeByDay) {
        this.reason = reason;
        this.timeByDay = timeByDay;
    }

    public TimeTable() {
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public TimeByDay getTimeByDay() {
        return timeByDay;
    }

    public void setTimeByDay(TimeByDay timeByDay) {
        this.timeByDay = timeByDay;
    }

    /**/
    public static class Reason {
        private String name;
        private boolean fullDay;
        private Long reasonId;

        public boolean isFullDay() {
            return fullDay;
        }

        public void setFullDay(boolean fullDay) {
            this.fullDay = fullDay;
        }

        public Long getReasonId() {
            return reasonId;
        }

        public void setReasonId(Long reasonId) {
            this.reasonId = reasonId;
        }

        public Reason(String name, boolean fullDay, Long reasonId) {

            this.name = name;
            this.fullDay = fullDay;
            this.reasonId = reasonId;
        }

        public Reason() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**/
    public static class TimeByDay {

        List<Time> time;

        public TimeByDay(List<Time> time) {
            this.time = time;
        }

        public TimeByDay() {
        }

        public List<Time> getTime() {
            return time;
        }

        public void setTime(List<Time> time) {
            this.time = time;
        }

        /**/
        public static class Time {

            private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

            private LocalTime summary;
            List<TimePair> timePairs;
            private LocalDate absenceDate;
            private Long absenceId;

            public LocalDate getAbsenceDate() {
                return absenceDate;
            }

            public void setAbsenceDate(LocalDate absenceDate) {
                this.absenceDate = absenceDate;
            }

            public Long getAbsenceId() {
                return absenceId;
            }

            public void setAbsenceId(Long absenceId) {
                this.absenceId = absenceId;
            }

            public Time(LocalTime summary, List<TimePair> timePairs, LocalDate absenceDate, Long absenceId) {
                this.summary = summary;
                this.timePairs = timePairs;
                this.absenceDate = absenceDate;
                this.absenceId = absenceId;
            }

            public List<TimePair> getTimePairs() {
                return timePairs;
            }

            public void setTimePairs(List<TimePair> timePairs) {
                this.timePairs = timePairs;
            }

            public Time() {
            }

            public LocalTime getSummary() {
                return summary;
            }

            public void setSummary(LocalTime summary) {
                this.summary = summary;
            }



            public static class TimePair {
                private LocalDateTime from;
                private LocalDateTime to;

                public TimePair() {
                }

                public TimePair(LocalDateTime from, LocalDateTime to) {

                    this.from = from;
                    this.to = to;
                }

                public LocalDateTime getFrom() {

                    return from;
                }

                public void setFrom(LocalDateTime from) {
                    this.from = from;
                }

                public LocalDateTime getTo() {
                    return to;
                }

                public void setTo(LocalDateTime to) {
                    this.to = to;
                }

                public String getStart() {
                    return from != null ? from.format(DATE_TIME_FORMATTER) : "";
                }

                public String getFinish() {
                    return to != null ? to.format(DATE_TIME_FORMATTER) : "";
                }
            }
        }
    }
}
