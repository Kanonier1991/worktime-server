package springapp.dto.view;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Igor on 01.05.2017.
 */
public class TimeJournal {

    private List<String> header;
    private List<TimeTable> timeTable;

    public TimeJournal(List<String> header, List<TimeTable> timeTable) {
        this.header = header;
        setTimeTable(timeTable);
    }

    public TimeJournal() {
    }

    public List<String> getHeader() {
        return header;
    }

    public void setHeader(List<String> header) {
        this.header = header;
    }

    public List<TimeTable> getTimeTable() {

        return timeTable;
    }

    public void setTimeTable(List<TimeTable> timeTable) {
        this.timeTable = timeTable;
        timeTable.sort((o1, o2) -> {
            int reason1Id = Math.toIntExact(o1.getReason().getReasonId() != null ? o1.getReason().getReasonId() : Integer.MIN_VALUE);
            int reason2Id = Math.toIntExact(o2.getReason().getReasonId() != null ? o2.getReason().getReasonId() : Integer.MIN_VALUE);
            return reason2Id - reason1Id;
        });
    }
}
