package springapp.services.importer;

import springapp.dto.Employee;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by Igor on 20.05.2017.
 */
public interface TimeImporter {

    String requestEmployeeData(Employee employee, LocalDate from, LocalDate to) throws IOException;

    void doImportAll(LocalDate from, LocalDate to);
}

