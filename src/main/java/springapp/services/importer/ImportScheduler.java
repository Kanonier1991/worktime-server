package springapp.services.importer;

/**
 * Created by Igor on 20.05.2017.
 */
public interface ImportScheduler {

    void startImport();
}
