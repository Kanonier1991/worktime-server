package springapp.services.importer.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import springapp.dto.Employee;
import springapp.dto.importer.ImportingTime;
import springapp.services.importer.DocumentParser;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * Created by Igor on 20.05.2017.
 */
@Service
public class HtmlDocumentParserImpl implements DocumentParser {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    @Override
    public List<ImportingTime.Day> parseTimePage(String pageHtml) {
        Document html = Jsoup.parse(pageHtml);
        /*Check for existing <h2 align="center">Отчет посещаемости рабочего места</h2>*/
        if (!html.getElementsByTag("h2").hasText()) {
            return Collections.emptyList();
        }
        Element timeTable = html.getElementsByTag("table").get(1);
        Elements rows = timeTable.getElementsByTag("tr");
        List<ImportingTime.Day> days = new ArrayList<>();

        IntStream.range(1, rows.size() - 2).forEach(rowNumber -> {
            Element row = rows.get(rowNumber);
            Elements cells = row.getElementsByTag("td");
            String times = cells.get(4).text();
            List<ImportingTime.AttendanceTime> attendanceTimes = new ArrayList<>();
            ImportingTime.Day day = new ImportingTime.Day();
            if (!StringUtils.isEmpty(times)) {
                String date = cells.get(1).text();
                LocalDate rowDate = LocalDate.from(DATE_FORMATTER.parse(date));
                day.setDate(rowDate);
                Pattern pattern = Pattern.compile("(П|У)[0-9]{2}:[0-9]{2}");
                Matcher matcher = pattern.matcher(times);
                while (matcher.find()) {
                    String attendanceTime = times.substring(matcher.start(), matcher.end());
                    if (attendanceTime.startsWith("П")) {
                        LocalTime inputTime = LocalTime.from(TIME_FORMATTER.parse(attendanceTime.replaceAll("П", "")));
                        attendanceTimes.add(new ImportingTime.AttendanceTime("П", inputTime));
                    } else {
                        LocalTime outputTime = LocalTime.from(TIME_FORMATTER.parse(attendanceTime.replaceAll("У", "")));
                        attendanceTimes.add(new ImportingTime.AttendanceTime("У", outputTime));
                    }
                }

                List<ImportingTime.Time> importingTimeForDay = new ArrayList<>();
                for (int i = 0; i < attendanceTimes.size(); i++) {
                    ImportingTime.AttendanceTime attendanceTime = attendanceTimes.get(i);
                    ImportingTime.Time time = new ImportingTime.Time();
                    if ("П".equals(attendanceTime.getType())) {
                        time.setStart(attendanceTime.getTime());
                        if ("У".equals(attendanceTimes.get(i + 1).getType())) {
                            time.setFinish(attendanceTimes.get(i + 1).getTime());
                            i++;
                        }
                    } else {
                        time.setFinish(attendanceTime.getTime());
                    }
                    importingTimeForDay.add(time);
                }
                day.setTimes(importingTimeForDay);
            }
            if (day.getDate() != null) {
                days.add(day);
            }
        });
        return days;
    }
}
