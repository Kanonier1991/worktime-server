package springapp.services.importer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import springapp.core.Utils;
import springapp.dto.Employee;
import springapp.dto.ExternalAttendance;
import springapp.dto.importer.ImportingTime;
import springapp.repositories.EmployeeRepository;
import springapp.repositories.ExternalAttendanceRepository;
import springapp.services.importer.DocumentParser;
import springapp.services.importer.ImportScheduler;
import springapp.services.importer.TimeImporter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by Igor on 20.05.2017.
 */
@Component
public class ImportSchedulerImpl implements ImportScheduler {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private TimeImporter timeImporter;

    // Cron expression: seconds/minutes/hours/day_of_month/month/day_of_week/year(optional)
    @Scheduled(cron = "0 10 4  * * 7 ")
    public void startImport() {
        System.out.println("Start " + LocalDateTime.now());

        /*Import data for two weeks ago.*/
        LocalDate startTime = Utils.getDatesOfWeek(LocalDate.now().minusDays(14)).get(0);
        LocalDate finishTime = Utils.getDatesOfWeek(LocalDate.now().minusDays(14)).get(0);

        timeImporter.doImportAll(startTime, finishTime);
    }
}
