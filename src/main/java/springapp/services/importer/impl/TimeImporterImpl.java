package springapp.services.importer.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import springapp.core.Utils;
import springapp.dto.Employee;
import springapp.dto.ExternalAttendance;
import springapp.dto.importer.ImportingTime;
import springapp.repositories.EmployeeRepository;
import springapp.repositories.ExternalAttendanceRepository;
import springapp.services.core.HttpService;
import springapp.services.importer.DocumentParser;
import springapp.services.importer.TimeImporter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Igor on 20.05.2017.
 */
@Service
public class TimeImporterImpl implements TimeImporter {

    private final String HOST = "https://tele-pass.ru";
    private final String LOGIN_URL = HOST + "/login.php";
    private final String REPORTS_URL = HOST + "/report01.php";

    @Autowired
    private HttpService httpService;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DocumentParser documentParser;
    @Autowired
    private ExternalAttendanceRepository externalAttendanceRepository;

    @Override
    public String requestEmployeeData(Employee employee, LocalDate from, LocalDate to) throws IOException {
        HttpGet get = new HttpGet(LOGIN_URL);
        HttpResponse response = httpService.get(get);

        HttpPost post = new HttpPost(LOGIN_URL);
        post.setHeader("Cookie", response.getHeaders("Set-Cookie")[0].getValue());
        post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        post.setHeader("Accept-Encoding", "gzip, deflate, br");
        List<BasicNameValuePair> data = new ArrayList<>();
        data.add(new BasicNameValuePair("id", employee.getSystemId()));
        data.add(new BasicNameValuePair("username", employee.getExternalName()));
        data.add(new BasicNameValuePair("password", employee.getPassword()));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(data, "UTF-8");
        post.setEntity(entity);
        HttpResponse postResponse = httpService.post(post);

        HttpPost postReport = new HttpPost(REPORTS_URL);
        List<BasicNameValuePair> postReportData = new ArrayList<>();
        postReportData.add(new BasicNameValuePair("timetype", "3"));

        //from
        postReportData.add(new BasicNameValuePair("day1_r3", String.valueOf(from.getDayOfMonth())));
        postReportData.add(new BasicNameValuePair("month1_r3", String.valueOf(from.getMonthValue())));
        postReportData.add(new BasicNameValuePair("year1_r3", String.valueOf(from.getYear())));

        //to
        postReportData.add(new BasicNameValuePair("day2_r3", String.valueOf(to.getDayOfMonth())));
        postReportData.add(new BasicNameValuePair("month2_r3", String.valueOf(to.getMonthValue())));
        postReportData.add(new BasicNameValuePair("year2_r3", String.valueOf(to.getYear())));

        postReportData.add(new BasicNameValuePair("allday", "on"));
        postReportData.add(new BasicNameValuePair("sort", "01"));
        UrlEncodedFormEntity postReportDataEntity = new UrlEncodedFormEntity(postReportData, "UTF-8");
        postReport.setHeader("Cookie", response.getHeaders("Set-Cookie")[0].getValue());
        postReport.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        postReport.setHeader("Accept-Encoding", "gzip, deflate, br");
        postReport.setEntity(postReportDataEntity);
        httpService.post(postReport);


        HttpGet getReport = new HttpGet(HOST + "/table01.php");
        getReport.setHeader("Cookie", response.getHeaders("Set-Cookie")[0].getValue());
        getReport.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        getReport.setHeader("Accept-Encoding", "gzip, deflate, br");
        HttpResponse tableResponse = httpService.get(getReport);
        return IOUtils.toString(tableResponse.getEntity().getContent(), "windows-1251");
    }

    @Override
    public void doImportAll(LocalDate from, LocalDate to) {
        List<Employee> employees = employeeRepository.findByActive(true);
        employees.forEach(employee -> {
            if (!StringUtils.isEmpty(employee.getSystemId())
                    && !StringUtils.isEmpty(employee.getExternalName())
                    && !StringUtils.isEmpty(employee.getPassword())) {
                try {
                    String pageHtml = requestEmployeeData(employee, from , to);
                    List<ImportingTime.Day> days = documentParser.parseTimePage(pageHtml);
                    List<LocalDateTime> existingStarts = externalAttendanceRepository.findByEmployeeAndStartBetween(
                            employee,
                            from.atTime(LocalTime.MIN),
                            to.atTime(LocalTime.MAX)).stream()
                            .map(externalAttendance -> externalAttendance.getStart())
                            .collect(Collectors.toList());
                    List<LocalDateTime> existingFinishes = externalAttendanceRepository.findByEmployeeAndFinishBetween(
                            employee,
                            from.atTime(LocalTime.MIN),
                            to.atTime(LocalTime.MAX)).stream()
                            .map(externalAttendance -> externalAttendance.getFinish())
                            .collect(Collectors.toList());
                    days.forEach(day -> {
                        day.getTimes().forEach(time -> {
                            ExternalAttendance externalAttendance = new ExternalAttendance();
                            externalAttendance.setEmployee(employee);
                            if (time.getStart() != null ) {
                                externalAttendance.setStart(day.getDate().atTime(time.getStart()));
                            }
                            if (time.getFinish() != null) {
                                externalAttendance.setFinish(day.getDate().atTime(time.getFinish()));
                            }
                            if ((time.getStart() != null &&!existingStarts.contains(day.getDate().atTime(time.getStart())))
                                    || (time.getFinish() != null &&!existingStarts.contains(day.getDate().atTime(time.getFinish())))) {
                                externalAttendanceRepository.save(externalAttendance);
                            }
                        });
                    });
                    System.out.println(days.size());
                    System.out.println(LocalDateTime.now());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
