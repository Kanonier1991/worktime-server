package springapp.services.importer;

import springapp.dto.Employee;
import springapp.dto.importer.ImportingTime;

import java.util.List;

/**
 * Created by Igor on 20.05.2017.
 */
public interface DocumentParser {

    List<ImportingTime.Day> parseTimePage(String pageHtml);
}
