package springapp.services.core;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.io.IOException;

/**
 * Created by Igor on 20.05.2017.
 */
public interface HttpService {

    HttpResponse get(HttpGet get) throws IOException;
    HttpResponse post(HttpPost post) throws IOException;
}
