package springapp.services.core;

import springapp.dto.Absence;
import springapp.dto.Employee;
import springapp.dto.view.TimeTable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Igor on 30.04.2017.
 */
public interface EmployeeService {

    boolean addReasonToEmployee(Long employeeId, Long reasonId);

    Employee findByIdOrIp(Long id, String ip);

    List<Absence> findEmployeeAbsenceForInterval(Employee employee, LocalDateTime from, LocalDateTime to);

    void addAbsenceForEmployee(Long employeeId, Long reasonId, LocalDateTime from, LocalDateTime to);

    List<TimeTable> getEmployeeTimeJournal(Employee employee, LocalDate currentDate);
}
