package springapp.services.core.impl;

import static springapp.core.Utils.getWeekDay;
import static springapp.core.Utils.getDatesOfWeek;
import static springapp.core.Utils.getDatesForInterval;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import springapp.dto.*;
import springapp.dto.view.TimeTable;
import springapp.repositories.AbsenceReasonRepository;
import springapp.repositories.AbsenceRepository;
import springapp.repositories.EmployeeRepository;
import springapp.repositories.ExternalAttendanceRepository;
import springapp.services.core.AttendanceService;
import springapp.services.core.EmployeeService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor on 30.04.2017.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AbsenceReasonRepository reasonRepository;
    @Autowired
    private AbsenceRepository absenceRepository;
    @Autowired
    private AttendanceService attendanceService;
    @Autowired
    private ExternalAttendanceRepository externalAttendanceRepository;

    @Override
    public boolean addReasonToEmployee(Long employeeId, Long reasonId) {
        Employee employee = employeeRepository.findOne(employeeId);
        AbsenceReason reason = reasonRepository.findOne(reasonId);
        employee.getEmployeeReasons().add(reason);
        employeeRepository.save(employee);
        return false;
    }

    @Override
    public Employee findByIdOrIp(Long id, String ip) {
        if (id != null) {
            return employeeRepository.findOne(id);
        } else if (!StringUtils.isEmpty(ip)) {
            return employeeRepository.findByIp(ip);
        }
        return null;
    }

    @Override
    public List<Absence> findEmployeeAbsenceForInterval(Employee employee, LocalDateTime from, LocalDateTime to) {
        LocalDateTime start = from != null ? from : getWeekDay(LocalDate.now(), 1).atTime(LocalTime.MIN);
        LocalDateTime finish = to != null ? to : getWeekDay(LocalDate.now(), 7).atTime(LocalTime.MAX);
        return absenceRepository.findAbsencesByEmployeeAndStartAfterAndFinishBefore(employee, start, finish);
    }



    @Override
    public void addAbsenceForEmployee(Long employeeId, Long reasonId, LocalDateTime from, LocalDateTime to) {
        Employee employee = employeeRepository.findOne(employeeId);
        AbsenceReason reason = reasonRepository.findOne(reasonId);
        if (reason.isFullDay()) {
            getDatesForInterval(from.toLocalDate(), to.toLocalDate()).forEach(localDate -> {
                Absence absence = new Absence();
                absence.setEmployee(employee);
                absence.setReason(reason);
                absence.setStart(localDate.atTime(10,0));
                absence.setFinish(localDate.atTime(18,0));
                absenceRepository.save(absence);
            });
            return;
        }
        Absence absence = new Absence(employee, from, to, reason);
        absenceRepository.save(absence);
    }

    @Override
    public List<TimeTable> getEmployeeTimeJournal(Employee employee, LocalDate currentDate) {
        List<LocalDate> weekDays = getDatesOfWeek(currentDate);

        /*Build attendance time table row*/
        TimeTable attendanceTimes = new TimeTable();
        attendanceTimes.setReason(new TimeTable.Reason("Время персонального компьютера", false, -1L));
        List<TimeTable.TimeByDay.Time> reasonTimes = new ArrayList<>();
        weekDays.forEach(localDate -> {
            TimeTable.TimeByDay.Time timeForDay = new TimeTable.TimeByDay.Time();
            List<Attendance> attendances = attendanceService.getAttendances(employee, localDate.atTime(LocalTime.MIN), localDate.atTime(LocalTime.MAX));
            if (attendances.size() > 0) {
                List<TimeTable.TimeByDay.Time.TimePair>  timePairs = new ArrayList<>();
                int countTime = 0;
                for(Attendance attendance : attendances) {
                    LocalDateTime start = attendance.getStart();
                    LocalDateTime finish = attendance.getFinish();
                    TimeTable.TimeByDay.Time.TimePair timePair = new TimeTable.TimeByDay.Time.TimePair(start, finish);
                    timePairs.add(timePair);
                    if (finish != null && start != null) {
                        countTime += (finish.getHour() - start.getHour()) * 60 + finish.getMinute() - start.getMinute();
                    }
                }
                timeForDay.setSummary(LocalTime.of(
                        countTime/60,
                        countTime - (countTime/60)*60
                        )
                );
                timeForDay.setTimePairs(timePairs);
                timeForDay.setAbsenceDate(localDate);
            }
            reasonTimes.add(timeForDay);
        });
        attendanceTimes.setTimeByDay(new TimeTable.TimeByDay(reasonTimes));

        List<TimeTable> timeJournal = new ArrayList<>();
        timeJournal.add(attendanceTimes);


        /*Build control time table row*/
        TimeTable controlTimes = new TimeTable();
        controlTimes.setReason(new TimeTable.Reason("Данные пропускного пункта", false, -2L));
        List<TimeTable.TimeByDay.Time> controlReasonTimes = new ArrayList<>();
        weekDays.forEach(localDate -> {
            TimeTable.TimeByDay.Time timeForDay = new TimeTable.TimeByDay.Time();
            List<ExternalAttendance> externalAttendances = externalAttendanceRepository.findByEmployeeAndStartBetween(employee, localDate.atTime(LocalTime.MIN), localDate.atTime(LocalTime.MAX));
            if (externalAttendances.size() > 0) {
                List<TimeTable.TimeByDay.Time.TimePair>  timePairs = new ArrayList<>();
                int countTime = 0;
                for(ExternalAttendance attendance : externalAttendances) {
                    LocalDateTime start = attendance.getStart();
                    LocalDateTime finish = attendance.getFinish();
                    TimeTable.TimeByDay.Time.TimePair timePair = new TimeTable.TimeByDay.Time.TimePair(start, finish);
                    timePairs.add(timePair);
                    if (finish != null && start != null) {
                        countTime += (finish.getHour() - start.getHour()) * 60 + finish.getMinute() - start.getMinute();
                    }
                }
                timeForDay.setSummary(LocalTime.of(
                        countTime/60,
                        countTime - (countTime/60)*60
                        )
                );
                timeForDay.setTimePairs(timePairs);
                timeForDay.setAbsenceDate(localDate);
            }
            controlReasonTimes.add(timeForDay);
        });
        controlTimes.setTimeByDay(new TimeTable.TimeByDay(controlReasonTimes));

        timeJournal.add(controlTimes);

        /*Build absence time table rows*/
        employee.getEmployeeReasons().forEach(absenceReason -> {
            TimeTable timeTable = new TimeTable();
            timeTable.setReason(new TimeTable.Reason(absenceReason.getName(), absenceReason.isFullDay(), absenceReason.getId()));
            TimeTable.TimeByDay timeByDay = new TimeTable.TimeByDay();
            List<TimeTable.TimeByDay.Time> reasonDates = new ArrayList<>();
            weekDays.forEach(day -> {
                List<Absence> absences = absenceRepository.findAbsencesByEmployeeAndReasonAndStartAfterAndFinishBefore(
                        employee,
                        absenceReason,
                        day.atTime(LocalTime.MIN),
                        day.atTime(LocalTime.MAX)
                );

                TimeTable.TimeByDay.Time time = new TimeTable.TimeByDay.Time();
                if (absences != null && !absences.isEmpty()) {
                    List<TimeTable.TimeByDay.Time.TimePair>  timePairs = new ArrayList<>();
                    if (absenceReason.isFullDay()) {
                        TimeTable.TimeByDay.Time.TimePair timePair = new TimeTable.TimeByDay.Time.TimePair(absences.get(0).getStart(), absences.get(0).getFinish());
                        timePairs.add(timePair);
                    } else {
                        int countTime = 0;
                        for(Absence absence : absences) {
                            LocalDateTime start = absence.getStart();
                            LocalDateTime finish = absence.getFinish();
                            TimeTable.TimeByDay.Time.TimePair timePair = new TimeTable.TimeByDay.Time.TimePair(start, finish);
                            timePairs.add(timePair);
                            countTime += (finish.getHour() - start.getHour()) * 60 + finish.getMinute() - start.getMinute();
                        }
                        time.setSummary(LocalTime.of(
                                countTime/60,
                                countTime - (countTime/60)*60
                                )
                        );
                    }
                    time.setTimePairs(timePairs);
                    time.setAbsenceId(absences.get(0).getId());
                }
                time.setAbsenceDate(day);
                reasonDates.add(time);
            });
            timeByDay.setTime(reasonDates);
            timeTable.setTimeByDay(timeByDay);
            timeJournal.add(timeTable);
        });
        return timeJournal;
    }
}
