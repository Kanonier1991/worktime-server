package springapp.services.core.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springapp.dto.Attendance;
import springapp.dto.Employee;
import springapp.repositories.AttendanceRepository;
import springapp.repositories.EmployeeRepository;
import springapp.services.core.AttendanceService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by Igor on 14.04.2017.
 */
@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceRepository attendanceRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Attendance save(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        Employee employee = employeeRepository.findByIp(ip);
        List<Attendance> attendances = attendanceRepository.findAttendancesByEmployeeAndStartAfterAndFinishBeforeOrderByStart(
                employee,
                getStarteDay(),
                getEndDay()
        );
        if (!attendances.isEmpty()) {
            attendances.forEach(attendance -> {
                if (attendance.getFinish() == null) {
                    attendance.setFinish(LocalDateTime.now());
                    attendanceRepository.save(attendance);
                }
            });
        }

        Attendance attendance = new Attendance(employee,
                                               LocalDateTime.now(),
                                               null,
                                              "Компьютер включен");
        attendanceRepository.save(attendance);
        return null;
    }

    @Override
    public List<Attendance> update(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        Employee employee = employeeRepository.findByIp(ip);
        List<Attendance> attendances = attendanceRepository.findAttendancesByEmployeeAndFinish(
                employee,
                null
        );
        attendances.forEach(attendance -> {
            attendance.setFinish(LocalDateTime.now());
            attendanceRepository.save(attendance);
        });

        return null;
    }

    @Override
    public List<Attendance> getAttendances(Employee employee, LocalDateTime from, LocalDateTime to) {
        LocalDateTime start = from != null ? from : LocalDateTime.of(2000, 1, 1,1,1);
        LocalDateTime finish = to != null ? to : LocalDateTime.now();
        return attendanceRepository.findAttendancesByEmployeeAndStartAfterAndFinishBeforeOrderByStart(
                employee,
                start,
                finish
        );
    }

    @Override
    public List<Attendance> getTodaysAttendances(HttpServletRequest request) {
        Employee employee = employeeRepository.findByIp(request.getRemoteAddr());
        return attendanceRepository.findAttendancesByEmployeeAndStartAfterAndFinishBeforeOrderByStart(employee, getStarteDay(), getEndDay());
    }

    @Override
    public List<Attendance> lockSystem(String ip) {
        Employee employee = employeeRepository.findByIp(ip);
        List<Attendance> attendances = attendanceRepository.findAttendancesByEmployeeAndFinish(
                employee,
                null
        );
        attendances.forEach(attendance -> {
            attendance.setFinish(LocalDateTime.now());
            attendanceRepository.save(attendance);
        });
        return null;
    }

    @Override
    public Attendance unlockSystem(String ip) {
        Employee employee = employeeRepository.findByIp(ip);
        List<Attendance> attendances = attendanceRepository.findAttendancesByEmployeeAndStartAfterAndFinishBeforeOrderByStart(
                employee,
                getStarteDay(),
                getEndDay()
        );
        if (!attendances.isEmpty()) {
            attendances.forEach(attendance -> {
                if (attendance.getFinish() == null) {
                    attendance.setFinish(LocalDateTime.now());
                    attendanceRepository.save(attendance);
                }
            });
        }

        Attendance attendance = new Attendance(employee,
                LocalDateTime.now(),
                null,
                "Система разблокирована");
        attendanceRepository.save(attendance);
        return null;
    }

    private LocalDateTime getStarteDay() {
        return LocalDate.now().atTime(LocalTime.MIN);
    }

    private LocalDateTime getEndDay() {
        return LocalDate.now().atTime(LocalTime.MAX);
    }
}
