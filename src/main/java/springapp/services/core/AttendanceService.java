package springapp.services.core;

import springapp.dto.Attendance;
import springapp.dto.Employee;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Igor on 14.04.2017.
 */
public interface AttendanceService {

    Attendance save(HttpServletRequest request);

    List<Attendance> update(HttpServletRequest request);

    List<Attendance> getAttendances(Employee employee, LocalDateTime from, LocalDateTime to);

    List<Attendance> getTodaysAttendances(HttpServletRequest request);

    List<Attendance> lockSystem(String ip);

    Attendance unlockSystem(String ip);
}
