package springapp.core;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

/**
 * Created by Igor on 01.05.2017.
 */
public class Utils {

    public static LocalDate getWeekDay(LocalDate date, int dayOfWeek) {
        return date.with(WeekFields.of(Locale.FRANCE).dayOfWeek(), dayOfWeek);
    }

    public static List<LocalDate> getDatesOfWeek(LocalDate date) {
        List<LocalDate> dates = new ArrayList<>();
        IntStream.range(1, 8).forEach(dayNumber -> {
            dates.add(getWeekDay(date, dayNumber));
        });
        return dates;
    }

    public static List<LocalDate> getDatesForInterval(LocalDate start, LocalDate finish) {
        List<LocalDate> result = new ArrayList<>();
        LocalDate addedDate = start;
        result.add(addedDate.plusDays(0));
        while (!addedDate.equals(finish)) {
            addedDate = addedDate.plusDays(1);
            result.add(addedDate.plusDays(0));
        }

        return result;
    }
}
