/**
 * Created by Igor on 30.04.2017.
 */

var EmployeeList = {

    $element: {},

    init: function(element) {
        this.$element = element;
        var employees = this.$element.getElementsByClassName('employes-list__table-row');
        for (let i = 0; i<employees.length; i++) {
            let employeeRow = employees[i];
            let empId = employeeRow.dataset.empid;
            employeeRow.onclick = () =>  window.location = '/employee/info?empid=' + empId;
        }
    }
}

App.register('EmployeeList', EmployeeList);