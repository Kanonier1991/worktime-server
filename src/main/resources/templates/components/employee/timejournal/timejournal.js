var AdminTimeJournal = {

    init: (element, config) => {
        let weekSelector = element.querySelectorAll('.timejournal__week-selecter input');
        weekSelector[0].onchange = (event) => {
            window.location = '/employee/info?week='
                + event.currentTarget.value
                + '&empid='
                + config;
        }
    }
}

App.register('AdminTimeJournal', AdminTimeJournal);