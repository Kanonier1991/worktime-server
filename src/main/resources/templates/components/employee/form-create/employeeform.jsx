var EmployeeForm = React.createClass({

    componentWillMount: function() {
        this.setState({positions: [], absencereason: [], employee: {}});
        this.loadPositions();
        this.loadReasons();
        let employeeId = this.props.employeeId;
        if (!!employeeId) {
            this.loadEmployee(employeeId);
        }
    },

    loadPositions: function () {
        var self = this;
        fetch('/position/get_all').then((response) => {
            return response.json()
        }).then((positions) => {
            let newState = Object.assign(self.state, {positions: positions});
            self.setState(newState);
        })
    },

    loadReasons: function () {
        var self = this;
        fetch('/absencereason/get_all').then((response) => {
            return response.json()
        }).then((absencereason) => {
            let newState = Object.assign(self.state, {absencereason: absencereason});
            self.setState(newState);
        })
    },

    loadEmployee: function (employeeId) {
        var self = this;
        fetch('/employee/get?employeeId=' + employeeId).then((response) => {
            return response.json()
        }).then((employee) => {
            let newState = Object.assign(self.state, {employee: employee});
            self.setState(newState);
        })
    },

    submitform: function (event) {
        event.preventDefault();
        let form = event.currentTarget;
        let employeeId = form.elements['employeeId'].value;
        let name = form.elements['name'].value;
        let ip = form.elements['ip'].value;
        let position = form.elements['position'].value;
        let systemId = form.elements['systemId'].value;
        let externalName = form.elements['externalName'].value;
        let externalPass = form.elements['externalPass'].value;

        var params = new FormData();

        params.append('employeeId', employeeId);
        params.append('position', position);
        params.append('name', name);
        params.append('ip', ip);
        params.append('systemId', systemId);
        params.append('externalName', externalName);
        params.append('externalPass', externalPass);
        fetch('/employee/create', {
            method: 'post',
            body: params
        }).then((response) => {
            location.reload();
        });
    },

    hidePopup: function(event) {
        let popup = event.currentTarget;
        popup.classList.add('popup--hidden');
    },

    render: function() {
        var self = this;
        var positions = self.state.positions.map((position) => {
            return <option value={position.id}>{position.fullName}</option>
        });
        let employee = this.state.employee;
        let title = !!employee.id ? 'Редактирование данных сотрудника' : 'Добавить нового пользователя'
        return(
            <div className="EmployeeForm-component" onClick={this.hidePopup}>
                <div className="create-employee-form" onClick={(event) => event.stopPropagation()}>
                    <div className="create-employee-form__title">
                        {title}:
                    </div>
                    <div className="create-employee-form__inputs">
                        <form onSubmit={self.submitform}>
                            <input type="hidden" name="employeeId" value={employee.id}/>
                            <div className="form-input"><input required type="text" placeholder="Имя" name="name" value={employee.name} onChange={(event) => {
                                let newEmployee = Object.assign({}, employee, {name: event.currentTarget.value});
                                self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                            }}/></div>
                            <div className="form-input"><input required type="text" placeholder="IP Адрес" name="ip" value={employee.ip} onChange={(event) => {
                                let newEmployee = Object.assign({}, employee, {ip: event.currentTarget.value});
                                self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                            }}/></div>
                            <div className="form-input"><input required type="text" placeholder="Id системы" name="systemId" value={employee.systemId} onChange={(event) => {
                                let newEmployee = Object.assign({}, employee, {systemId: event.currentTarget.value});
                                self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                            }}/></div>
                            <div className="form-input"><input required type="text" placeholder="Имя в tele-pass" name="externalName" value={employee.externalName} onChange={(event) => {
                                let newEmployee = Object.assign({}, employee, {externalName: event.currentTarget.value});
                                self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                            }}/></div>
                            <div className="form-input"><input required type="text" placeholder="Пароль в tele-pass" name="externalPass" value={employee.password} onChange={(event) => {
                                let newEmployee = Object.assign({}, employee, {externalPass: event.currentTarget.value});
                                self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                            }}/></div>
                            <label>Должность: </label>
                            <div className="form-input">
                                <select name="position" value={!!employee.position ? employee.position.id : ''} onChange={(event) => {
                                    let newEmployee = Object.assign({}, employee, {position: {id: event.currentTarget.value}});
                                    self.setState(Object.assign({}, self.state, {employee: newEmployee}));
                                    {id: event.currentTarget.value}
                                }}>
                                    {positions}
                                </select>
                            </div>
                            <button>Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});
