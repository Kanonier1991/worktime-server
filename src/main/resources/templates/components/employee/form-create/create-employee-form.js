var employeeFormComponent = {

    popupIsInitialized: false,
    popupElement: {},

    init: function(element, employee) {

        let employeeFormContainer = element.querySelector('.employee-form-container');
        let openPopupButtom = element.querySelector('.create-employee-form__open-popup-button');

        let employeeId = employee instanceof Object ? null : employee;

        openPopupButtom.textContent = !!employeeId ? 'Редактировать данные сотрудника' : 'Добавить нового сотрудника';
        openPopupButtom.onclick = () => {

            if (!this.popupIsInitialized) {
                ReactDOM.render(React.createElement(EmployeeForm, {employeeId: employeeId}), employeeFormContainer);
                this.popupElement = element.querySelector('.EmployeeForm-component');
                this.popupIsInitialized = true;
            } else {
                this.popupElement.classList.remove('popup--hidden');
            }
        }

    }
}

App.register("EmployeeFormComponent", employeeFormComponent);