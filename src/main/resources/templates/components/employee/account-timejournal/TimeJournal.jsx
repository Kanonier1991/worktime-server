var TimeJournal = React.createClass({

    componentWillMount: function() {
        this.setState(this.props);
    },

    loadJournal: function (event) {
        let week = event.currentTarget.dataset.week || event.currentTarget.value;
        let self = this;
        fetch('/employee/get_time_journal?week=' + week).then((response) => {
            return response.json();
        }).then((timeJournal) => {
            self.setState(Object.assign({}, timeJournal));
        });
    },

    openEditPopup: function (event) {
        let self = this;
        let dataSet = event.currentTarget.dataset;
        let fullDay = dataSet.fullday;
        let reasonid = dataSet.reasonid;
        if (fullDay === 'false' && !!reasonid) {
            let absenceDate = dataSet.absencedate;
            let params = 'reasonId=' + reasonid + '&absenceDate=' + absenceDate;
            fetch('/employee/get_absence_by_date?' + params).then((response) => {
                return response.json();
            }).then((timeList) => {
                self.setState(Object.assign({}, self.state, { popupTimes: timeList, popupVisible: true, popupData: {date: absenceDate, reasonId: reasonid}}));            });
        }
    },

    popupClickHandler: function () {
        this.setState(Object.assign({}, this.state, {popupVisible: false}))
    },

    submitAbsence: function (event) {
        let startHour = document.querySelector('#from_hour').value;
        let startMin = document.querySelector('#from_min').value;
        let finishHour = document.querySelector('#to_hour').value;
        let finishMin = document.querySelector('#to_min').value;
        let absenceDate = event.currentTarget.dataset.date;
        let reasonid = event.currentTarget.dataset.reasonid;

        if ((finishHour >= startHour || finishMin > startMin) &&
            finishHour < 24 && startHour < 24 &&
            finishMin < 60 && startMin < 60) {
            var params = new FormData();
            params.append('startHour', startHour);
            params.append('startMin', startMin);
            params.append('finishHour', finishHour);
            params.append('finishMin', finishMin);
            params.append('absenceDate', absenceDate);
            params.append('reasonid', reasonid);
            fetch('/employee/add_temporal_absence', {
                method: 'post',
                body: params
            }).then((response) => {
                return response.json();
            }).then((timeJournal) => {
                self.setState(Object.assign({}, timeJournal));
            });
        }
    },

    render: function () {
        let self = this;
        let headerProps = this.state.header;
        let reasonsRows = this.state.timeTable.map((item) => {
            return <ReasonRow openEditPopup={self.openEditPopup} reasonRow={item}/>
        });
        if (self.state.popupVisible !== undefined) {
            if (!!self.state.popupVisible) {
                document.querySelector('.time-detail-popup').style.display = 'block';
            } else {
                document.querySelector('.time-detail-popup').style.display = 'none';
            }
        }
        return (
            <div className="timejournal">
                <div className="timejournal__nav-panel">
                    <div className="timejournal__nav-panel-item">
                        <a href="#prev_week" onClick={self.loadJournal} data-week="prev">Предыдущая неделя</a>
                    </div>
                    <div className="timejournal__nav-panel-item">
                        <a href="#cur_week" onClick={self.loadJournal} data-week="current">Текущая неделя</a>
                    </div>
                    <div className="timejournal__nav-panel-item">
                        <a href="#next_week" onClick={self.loadJournal} data-week="next">Следующая неделя</a>
                    </div>
                </div>
                <div className="timejournal__week-selecter">
                    Выберите неделю: <input type="week" onChange={self.loadJournal}/>
                </div>
                <table className="timejournal__table">
                    <tbody>
                        <TimeJournalHeaderRow headerProps={headerProps}/>
                        {reasonsRows}
                    </tbody>
                </table>
                <Popup popupClickHandler={self.popupClickHandler} submitAbsence={self.submitAbsence} popupData={self.state.popupData} timeList={self.state.popupTimes}/>
            </div>
        );
    }
});

var TimeJournalHeaderRow = React.createClass({

    render: function () {
        let columns = this.props.headerProps.map((item) => {
            return <td className="timejournal__table-cell">{item}</td>
        });
        return(
            <tr className="timejournal__table-row--header">
                <td className="timejournal__table-cell">Причина</td>
                {columns}
            </tr>
        );
    }
});

var ReasonRow = React.createClass({

    timeToString: (timeEntity) => {
        if (!!timeEntity) {
            let time;
            if (!!timeEntity.time) {
                time = timeEntity.time;
            } else {
                time = timeEntity;
            }
            let hours = time.hour > 9 ? time.hour : '0' + time.hour;
            let minutes = time.minute > 9 ? time.minute : '0' + time.minute;
            return hours + ':' + minutes;
        } else {
            return '';
        }
    },

    getTdContent: function (item, fullDay, reasonId) {
        if (fullDay) {
            let checked = !!item.timePairs;
            let absenceDate = item.absenceDate.year + ':'
                + item.absenceDate.month + ':'
                + item.absenceDate.day;
            return <input type="checkbox"
                          checked={checked}
                          data-reasonId={reasonId}
                          data-absenceDate={absenceDate}
                          data-absenceId={item.absenceId}
                          onChange={this.fullDayUpdate}
                   />
        }
        return this.timeToString(item.summary);
    },

    fullDayUpdate: function (event) {
        let targetElement = event.currentTarget;
        let isTargetChecked = targetElement.checked;
        var params = new FormData();
        let dataSet = targetElement.dataset;
        params.append('absenceDate', dataSet.absencedate);
        params.append('reasonId', dataSet.reasonid);
        params.append('absenceId', dataSet.absenceid);
        params.append('addAbsence', isTargetChecked);
        fetch('/employee/update_absence', {
            method: 'post',
            body: params
        }).then((response) => {
            targetElement.checked = isTargetChecked;
            return response.json();
        }).then((absenceId) => {
            if (isTargetChecked) {
                targetElement.dataset.absenceid = absenceId;
            }
        });
    },

    render: function () {
        let self = this;
        let reason = this.props.reasonRow.reason;
        let fullDay = this.props.reasonRow.reason.fullDay;
        let reasonId = this.props.reasonRow.reason.reasonId;
        let dateColumns = this.props.reasonRow.timeByDay.time.map((item) => {

            let absenceDate;
            if (!!item.absenceDate) {
                absenceDate = item.absenceDate.year + ':'
                + item.absenceDate.month + ':'
                + item.absenceDate.day;
            }
            return (
                <td className="timejournal__table-cell"
                    data-fullday={fullDay}
                    data-reasonid={reasonId}
                    data-absenceDate={absenceDate}
                    onClick={self.props.openEditPopup}>
                    {self.getTdContent(item, fullDay, reasonId)}
                 </td>
            )
        });
        return(
            <tr className="timejournal__table-row">
                <td className="timejournal__table-cell">{reason.name}</td>
                {dateColumns}
            </tr>
        );
    }
});

var Popup = React.createClass({

    render: function () {
        let submitAbsence = this.props.submitAbsence;
        let properties = this.props.popupData;
        let popupClickHandler = this.props.popupClickHandler;
        let timePairs;
        if (!!this.props.timeList) {
            timePairs = this.props.timeList.map((item) => {
                return (
                    <div>{item.start} - {item.finish}</div>
                );
            });
        }
        return (

            <div onClick={popupClickHandler} className="time-detail-popup">

                <div className="time-detail-popup__content" onClick={(event) => event.stopPropagation()}>
                    <div className="time-detail-popup__content__title">Время:</div>
                    <div>
                        {timePairs}
                    </div>
                    <div>
                        <div className="time-label">Начало</div><input className="input-time" id="from_hour" defaultValue="10" pattern="\d{2}" type="textarea"/>ч:<input className="input-time" id="from_min" pattern="\d{2}" defaultValue="00" type="textarea"/>мин<br/>
                        <div className="time-label">Окончание</div><input className="input-time" id="to_hour" defaultValue="18" pattern="\d{2}" type="textarea"/>ч:<input className="input-time" id="to_min" pattern="\d{2}" defaultValue="00" type="textarea"/>мин<br/>
                    </div>
                    <div>
                        <a href="#" onClick={submitAbsence}
                            data-date={!!properties ? properties.date : ''}
                            data-reasonid={!!properties ? properties.reasonId : ''}>
                            Сохранить
                        </a>
                    </div>
                </div>
            </div>
        );
    }
});