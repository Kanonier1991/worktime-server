var AccountTimeJournal = {

    init: (element, config) => {

        ReactDOM.render(React.createElement(TimeJournal, config), element);
    }
}

App.register('AccountTimeJournal', AccountTimeJournal);