var employeeReasonsComponent = {

    popupIsInitialized: false,
    popupElement: {},

    init: function(element, employee) {

        let employeeResonsContainer = element.querySelector('.employee-reason-container');

        let employeeId = employee instanceof Object ? null : employee;
        ReactDOM.render(React.createElement(EmployeeReasons, {employeeId: employeeId}), employeeResonsContainer);

    }
}

App.register("EmployeeReasonsComponent", employeeReasonsComponent);