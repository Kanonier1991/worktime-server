var EmployeeReasons = React.createClass({

    componentWillMount: function() {
        this.setState({employeeReasons: [], allReasons: [], employeeId: this.props.employeeId});
        this.loadReasons();
        this.loadEmployeeReasons();
    },

    loadReasons: function () {
        var self = this;
        fetch('/absencereason/get_all').then((response) => {
            return response.json()
        }).then((absencereason) => {
            let newState = Object.assign(self.state, {allReasons: absencereason});
            self.setState(newState);
        })
    },

    loadEmployeeReasons: function () {
        var self = this;
        let param = "?empId=" + self.props.employeeId;
        fetch('/absencereason/get_by_employee' + param).then((response) => {
            return response.json()
        }).then((absencereason) => {
            let newState = Object.assign(self.state, {employeeReasons: absencereason});
            self.setState(newState);
        })
    },

    additableReasons: function () {
        return this.state.allReasons.filter((reason) => {
           return this.state.employeeReasons.map((item) => item.id).indexOf(reason.id) == -1;
        });
    },

    addReason: function (event) {
        var self = this;
        let select = document.querySelector("#add_reason_select");
        let addedId = select.value;
        var params = new FormData();

        params.append('empId', self.state.employeeId);
        params.append('reasonId', addedId);
        fetch('/employee/addReason', {
            method: 'post',
            body: params
        }).then((response) => {
            return response.json()
        }).then((absencereason) => {
            let newState = Object.assign(self.state, {employeeReasons: absencereason});
            self.setState(newState);
        });
    },

    removeReason: function (event) {

        var self = this;
        let removedId = event.currentTarget.dataset.reasonid;
        var params = new FormData();

        params.append('empId', self.state.employeeId);
        params.append('reasonId', removedId);
        fetch('/employee/removeReason', {
            method: 'post',
            body: params
        }).then((response) => {
            return response.json()
        }).then((absencereason) => {
            let newState = Object.assign(self.state, {employeeReasons: absencereason});
            self.setState(newState);
        });
    },

    render: function () {

        let self = this;
        let availableReasons = !!this.state.employeeReasons ? this.state.employeeReasons.map((item) => {
           return (
               <li>
                   <span className="employee-reasons-item">{item.name}</span>
                   <a href="#" onClick={self.removeReason} data-reasonid={item.id}>удалить</a>
               </li>
           );
        }) : '';

        let canBeAddedReasons = this.additableReasons().map(item => {
           return <option value={item.id}>{item.name}</option>
        });
        return (
            <div>
                <div>
                    Доступные причины отсутствия:
                    <ul>{availableReasons}</ul><br/>
                    Добавить причину:
                    <select id="add_reason_select">
                        {canBeAddedReasons}
                    </select> <a href="#" onClick={self.addReason}> Добавить</a>
                </div>
            </div>
        )
    }
});