var App = {

    components : {},

    start: function() {
        var foundComponents = document.querySelectorAll('[data-component]');
        foundComponents.forEach((foundComponent) => {
            let config = foundComponent.dataset.config;
            let configObj = !!config ? JSON.parse(config) : {};
            this.components[foundComponent.dataset.component].init(foundComponent, configObj)
        });

        console.log('Components initialized');
    },

    register: function(componentName,component) {
        this.components[componentName] = component;
    }
}